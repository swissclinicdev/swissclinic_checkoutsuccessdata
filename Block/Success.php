<?php

namespace Swissclinic\CheckoutSuccessData\Block;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Session;
use Magento\Framework\View\Element\Template;
use \Magento\Sales\Api\OrderRepositoryInterface;

class Success extends Template
{
    protected $_order;
    protected $_session;
    protected $_helper;
    protected $_orderRepository;
    protected $_storeManager;

    public function __construct(
        Context $context,
        Order $order,
        Session $session,
        OrderRepositoryInterface $orderRepository,
        array $data = []
    ) {
        $this->_order = $order;
        $this->_session = $session;
        $this->_orderRepository = $orderRepository;
        $this->_storeManager = $context->getStoreManager();
        parent::__construct($context, $data);
    }

    public function getOrder()
    {
        try {
            $orderId = $this->_session->getLastOrderId();
            return $this->_order->load($orderId);
        } catch (\Throwable $e) {
            $error = array('message' => $e->getMessage());
            $data = array('error' => $error);
            return json_encode($data, JSON_HEX_APOS);
        }
    }

    public function getOrderDataById($id)
    {
        return $this->_orderRepository->get($id);
    }
}